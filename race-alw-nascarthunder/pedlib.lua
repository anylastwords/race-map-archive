function Set(...)
  local set = {}
  for _, l in ipairs(arg) do set[l] = true end
  return set
end

c_MaxSkinId = 288
c_NoSkinIds = Set(0,1,2,3,4,5,6,8,42,65,74,86,119,149,208,239,265,266,267,268,269,270,271,272,273)

Animations = {
	sitting = {
		{ 'Attractors', 'Stepsit_loop' },
		{ 'MISC', 'SEAT_LR' },
		{ 'MISC', 'SEAT_watch' },
		{ 'MISC', 'Seat_talk_01' },
		{ 'MISC', 'Seat_talk_02' },
		{ 'BEACH', 'ParkSit_M_loop' },
		{ 'ped', 'SEAT_idle' },
		{ 'SUNBATHE', 'ParkSit_M_IdleA' },
		{ 'SUNBATHE', 'ParkSit_M_IdleC' }	
	},
	
	idling = {
		{ 'BAR', 'Barcustom_loop' },
		{ 'CLOTHES', 'CLO_Pose_Loop' },
		{ 'COP_AMBIENT', 'Coplook_loop' },
		{ 'COP_AMBIENT', 'Coplook_think' },
		{ 'MISC', 'Idle_Chat_02' },
		{ 'ON_LOOKERS', 'lkaround_loop' },
		{ 'OTB', 'wtchrace_loop' },
		{ 'ped', 'IDLE_chat' },
		{ 'ped', 'Idle_Gang1' },
		{ 'ped', 'XPRESSscratch' },
		{ 'ped', 'facgum' },
		{ 'ped', 'idlestance_old' },
		{ 'PLAYIDLES', 'shift' },
		{ 'PLAYIDLES', 'shldr' },
		{ 'PLAYIDLES', 'strleg' },
		{ 'PLAYIDLES', 'time' },
		{ 'RAPPING', 'RAP_A_Loop' },
		{ 'SMOKING', 'M_smk_drag' },
		{ 'SMOKING', 'M_smk_loop' },
		{ 'STRIP', 'PUN_LOOP' },
		{ 'SWEET', 'plyr_hndshldr_01' },
		{ 'WUZI', 'Wuzi_stand_loop' },
	},
	
	cheering = {
		{ 'ON_LOOKERS', 'panic_shout' },
		{ 'ON_LOOKERS', 'point_loop' },
		{ 'ON_LOOKERS', 'shout_01' },
		{ 'ON_LOOKERS', 'shout_02' },
		{ 'ON_LOOKERS', 'wave_loop' },
		{ 'OTB', 'wtchrace_win' },
		{ 'ped', 'FIGHTIDLE' },
		{ 'ped', 'endchat_03' },
		{ 'RIOT', 'RIOT_ANGRY_B' },
		{ 'RIOT', 'RIOT_CHANT' },
		{ 'RIOT', 'RIOT_challenge' },
		{ 'RIOT', 'RIOT_shout' },
		{ 'STRIP', 'PUN_CASH' },
		{ 'STRIP', 'PUN_HOLLER' }
	}

}


function createRandomPed(x, y, z, rz)
	local id = 1
	while c_NoSkinIds[id] do
		id = math.random(c_MaxSkinId)
	end
	
	local ped = createPed(id, x, y, z)
	setPedRotation(ped, rz)
	return ped
end


function animatePedRandom(ped, animCategory)
	local anims = Animations[animCategory];
	if not anims then return end
	
	local anim = anims[math.random(#anims)]
	
	setPedAnimation(ped, anim[1], anim[2])
end