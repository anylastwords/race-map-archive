
local root = getRootElement()


function createPedStrip(maxPeds, minX, maxX, minY, maxY, z, rz, category)
	local index = 1
	
	while index < maxPeds do
		local x = math.random(minX, maxX)
		local y = math.random(minY, maxY)
		local ped = createRandomPed(x, y, z, rz)
		if ped then
			animatePedRandom(ped, category)
		end
		index = index + 1
	end
end


function onMapStart()
	createPedStrip(5, 3457, 3466.5, -2913.5, -2893, 28, 270, "cheering") --L
	createPedStrip(5, 3457, 3466.5, -2884.5, -2863, 28, 270, "cheering") --L
	createPedStrip(5, 3457, 3466.5, -2855.5, -2837.5, 28, 270, "cheering") --L

	setRadioChannel(13)

	local snd1 = playSound3D("audio/cheer1.ogg", 3470, -2903, 23, true)
	setSoundMaxDistance(snd1, 60)

	local snd2 = playSound3D("audio/cheer2.ogg", 3470, -2874, 23, true)
	setSoundMaxDistance(snd2, 60)

	local snd3 = playSound3D("audio/cheer3.ogg", 3470, -2847, 23, true)
	setSoundMaxDistance(snd3, 60)

end
addEventHandler("onClientMapStarting", root, onMapStart)